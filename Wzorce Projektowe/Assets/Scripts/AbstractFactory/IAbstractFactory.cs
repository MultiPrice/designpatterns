using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory
{
    public interface IAbstractFactory
    {
        IAbstractProductA CreateProductA();
        IAbstractProductB CreateProductB();
    }
}
