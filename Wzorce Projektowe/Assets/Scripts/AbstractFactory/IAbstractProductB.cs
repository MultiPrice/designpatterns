using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory
{
    public interface IAbstractProductB
    {
        string UsageFunctionB();

        string Colaboration(IAbstractProductA collaborator);
    }
}