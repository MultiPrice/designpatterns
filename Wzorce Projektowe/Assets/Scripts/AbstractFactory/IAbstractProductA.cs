using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory
{
    public interface IAbstractProductA
    {
        string UsageFunctionA();
    }
}
