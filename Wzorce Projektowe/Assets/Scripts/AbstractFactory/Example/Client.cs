using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{
    public class Client : MonoBehaviour
    {
        private void Start()
        {
            Debug.Log("Client: Ordering weapons in novice blacksmith...");
            OrderWeapon(new NoviceBlacksmith());
         
            Debug.Log("Client: Ordering weapons in master blacksmith...");
            OrderWeapon(new MasterBlacksmith());
        }

        private void OrderWeapon(IAbstractFactory blacksmith)
        {
            IAbstractProductA productA = blacksmith.CreateProductA();
            IAbstractProductB productB = blacksmith.CreateProductB();

            Debug.Log(productB.UsageFunctionB());
            Debug.Log(productB.Colaboration(productA));
        }
    }
}
