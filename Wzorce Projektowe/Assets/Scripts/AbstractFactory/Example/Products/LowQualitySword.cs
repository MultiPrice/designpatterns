using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{
    public class LowQualitySword : IAbstractProductB
    {
        public string UsageFunctionB()
        {
            return "Usage of Low Quality Sword";
        }

        public string Colaboration(IAbstractProductA collaborator)
        {
            string result = collaborator.UsageFunctionA();

            return $"Dual wielding Low Quality Sword & ({result})";
        }
    }
}
