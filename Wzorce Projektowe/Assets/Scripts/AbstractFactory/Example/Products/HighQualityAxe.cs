using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{
    public class HighQualityAxe : IAbstractProductA
    {
        public string UsageFunctionA()
        {
            return "Usage of High Quality Axe";
        }
    }
}
