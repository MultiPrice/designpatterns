using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{ 
    public class LowQualityAxe : IAbstractProductA
    {
        public string UsageFunctionA()
        {
            return "Usage of Low Quality Axe";
        }
    }
}
