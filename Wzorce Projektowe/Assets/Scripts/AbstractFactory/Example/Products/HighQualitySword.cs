using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{
    public class HighQualitySword : IAbstractProductB
    {
        public string UsageFunctionB()
        {
            return "Usage of High Quality Sword";
        }

        public string Colaboration(IAbstractProductA collaborator)
        {
            string result = collaborator.UsageFunctionA();

            return $"Dual wielding High Quality Sword & ({result})";
        }
    }
}
