using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{
    public class MasterBlacksmith : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new HighQualityAxe();
        }

        public IAbstractProductB CreateProductB()
        {
            return new HighQualitySword();
        }
    }
}
