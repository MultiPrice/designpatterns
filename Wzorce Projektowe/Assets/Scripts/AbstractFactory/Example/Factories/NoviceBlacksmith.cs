using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.AbstractFactory.Examples
{
    public class NoviceBlacksmith : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new LowQualityAxe();
        }

        public IAbstractProductB CreateProductB()
        {
            return new LowQualitySword();
        }
    }
}
