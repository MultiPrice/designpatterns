Opis: Fabryka abstrakcyjna jest kreacyjnym wzorcem projektowym, 
	kt�ry pozwala tworzy� rodziny spokrewnionych ze sob� obiekt�w bez okre�lania ich konkretnych klas.

Zalety:
1. Zyskujesz pewno��, �e produkty, jakie otrzymujesz stosuj�c fabryk�, s� ze sob� kompatybilne.
2. Zapobiegasz �cis�emu sprz�gni�ciu konkretnych produkt�w z kodem klienckim.
3. Zasada pojedynczej odpowiedzialno�ci. Mo�esz zebra� kod kreacyjny produkt�w w jednym miejscu w programie, u�atwiaj�c tym samym p�niejsze utrzymanie kodu.
4. Zasada otwarte/zamkni�te. Mo�esz wprowadza� wsparcie dla nowych wariant�w produkt�w bez psucia istniej�cego kodu klienckiego.

Wady:
1. Kod mo�e sta� si� bardziej skomplikowany, ni� powinien. Wynika to z konieczno�ci wprowadzenia wielu nowych 
	interfejs�w i klas w toku wdra�ania tego wzorca projektowego.

Najlepsze zastosowania: 
1. Stosuj Fabryk� abstrakcyjn�, gdy tw�j kod ma dzia�a� na produktach z r�nych rodzin, 
	ale jednocze�nie nie chcesz, aby �ci�le zale�a� od konkretnych klas produkt�w. 
	Mog� one bowiem by� nieznane na wcze�niejszym etapie tworzenia programu, 
	albo chcesz umo�liwi� przysz�� rozszerzalno�� aplikacji.
2. Przemy�l ewentualn� implementacj� wzorca Fabryki abstrakcyjnej, 
	gdy masz do czynienia z klas� posiadaj�c� zestaw Metod wytw�rczych, 
	kt�re zbytnio przy�miewaj� g��wn� odpowiedzialno�� tej klasy.

Podsumowanie: Fabryka abstrakcyjna jest wzorcem podobnym do Metody wytw�rczej. Jej g��wna r�nica polega na tym, �e ka�da klasa tworz�ca mo�e
	wytwarza� wiele r�nych typ�w obiekt�w jak np. kilka rodzaj�w mieczy, tarcz lub pancerzy. Wszystko zarz�dzane jest przez konkrentych tw�rc�w
	pochodz�cych z jednej klasy dziedzicz�cej, wi�c podczas wywo�ywania u�ywa si� jednej klasy abstrakcyjnej, kt�ra wywo�uje odpowiednie 
	abstrakcyjne konstruktory od przedmiot�w. 