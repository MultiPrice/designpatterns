Opis: Budowniczy jest kreacyjnym wzorcem projektowym, kt�ry daje mo�liwo�� tworzenia z�o�onych obiekt�w etapami, krok po kroku. 
	Wzorzec ten pozwala produkowa� r�ne typy oraz reprezentacje obiektu u�ywaj�c tego samego kodu konstrukcyjnego.

Zalety:
1. Mo�esz konstruowa� obiekty etapami, odk�ada� niekt�re etapy, lub wykonywa� je rekursywnie.
2. Mo�esz wykorzysta� ponownie ten sam kod konstrukcyjny buduj�c kolejne reprezentacje produkt�w.
3. Zasada pojedynczej odpowiedzialno�ci. Mo�na odizolowa� skomplikowany kod konstrukcyjny od logiki biznesowej produktu.

Wady:
1. Kod staje si� bardziej skomplikowany, gdy� wdro�enie tego wzorca wi��e si� z dodaniem wielu nowych klas.

Najlepsze zastosowania: 
1. Stosuj wzorzec Budowniczy, aby pozby� si� �teleskopowych konstruktor�w�.
	np. class Pizza {
		Pizza(int size) { ... }
		Pizza(int size, boolean cheese) { ... }
		Pizza(int size, boolean cheese, boolean pepperoni) { ... }
2. Stosuj wzorzec Budowniczy, gdy potrzebujesz mo�liwo�ci tworzenia r�nych reprezentacji jakiego� produktu 
	(na przyk�ad, domy z kamienia i domy z drewna).
3. Stosuj ten wzorzec do konstruowania drzew Kompozytowych lub innych z�o�onych obiekt�w.

Podsumowanie: Wzorzec budowniczy ma swoje dobre zastosowania np. gdy w projekcie posiadamy wiele przeci��onych funkcji, kt�re przyjmuj� du�� ilo�� parametr�w.
	Dzi�ki temu wzorcowi mo�na wyeliminowa� te przeci��enia na rzecz struktury klasowej, kt�ra jest w stanie sprawi�, 
	�e rozwi�zanie stanie si� �atwiej rozwijalne i zarz�dzalne. Niestety pojawia si� tu problem z komplikowania rozwi�zania. 
	Wzorzec budowniczy jest tak�e bardzo przydatny, gdy tworzymy r�ne warianty podobnych konstrukcji, ktorych ka�dy poszczeg�lny element
	mo�e wyst�powa� w r�nych kombinacjach tworz�cych ca�o��. W tym przypadku wzorzec budowniczy jest bardzo przydatny, gdy� tworzenie konkretnych kombinacji 
	staje si� bardzo proste, ale tak samo jak przy zagnie�d�onych funkcjach powoduje to szybki rozrost i skomplikowanie tworzonego rozwi�zania. 