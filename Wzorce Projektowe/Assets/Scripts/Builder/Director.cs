using DesingPatterns.Builder;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.Builder
{
    public class Director
    {
        private IBuilder builder;

        public IBuilder Builder
        { 
            set 
            { 
                builder = value; 
            } 
        }

        public void BuildMinimalProduct()
        {
            builder.BuildPartA();
        }

        public void BuildFullProduct()
        {
            builder.BuildPartA();
            builder.BuildPartB();
            builder.BuildPartC();
        }
    }
}
