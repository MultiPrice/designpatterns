using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesingPatterns.Builder
{
    public interface IBuilder
    {
        void BuildPartA();

        void BuildPartB();

        void BuildPartC();
    }
}
