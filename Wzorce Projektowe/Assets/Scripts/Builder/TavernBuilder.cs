namespace DesingPatterns.Builder.Examples
{
    public class TavernBuilder : IBuilder
    {
        private Product product = new Product();

        TavernBuilder()
        {
            Reset();
        }

        private void Reset()
        {
            product = new Product();
        }

        public void BuildPartA()
        {
            product.Add("Tavern Dining room");
        }

        public void BuildPartB()
        {
            product.Add("Tavern Bathroom");
        }

        public void BuildPartC()
        {
            product.Add("Tavern Bedroom");
        }

        public Product GetProduct()
        {
            Product result = product;

            Reset();

            return result;
        }
    }
}