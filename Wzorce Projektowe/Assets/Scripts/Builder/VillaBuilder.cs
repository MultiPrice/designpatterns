namespace DesingPatterns.Builder.Examples
{
    public class VillaBuilder : IBuilder
    {
        private Product product = new Product();

        VillaBuilder()
        {
            Reset();
        }

        private void Reset()
        {
            product = new Product();
        }

        public void BuildPartA()
        {
            product.Add("Villa Diningroom");
        }

        public void BuildPartB()
        {
            product.Add("Villa Bathroom");
        }

        public void BuildPartC()
        {
            product.Add("Villa Bedroom");
        }

        public Product GetProduct()
        {
            Product result = product;

            Reset();

            return result;
        }
    }
}