namespace DesingPatterns.Builder.Examples
{
    public class CastleBuilder : IBuilder
    {
        private Product product = new Product();

        CastleBuilder()
        {
            Reset();
        }

        private void Reset()
        {
            product = new Product();
        }

        public void BuildPartA()
        {
            product.Add("Castle Dining room");
        }

        public void BuildPartB()
        {
            product.Add("Castle Bathroom");
        }

        public void BuildPartC()
        {
            product.Add("Castle Bedroom");
        }

        public Product GetProduct()
        {
            Product result = product;

            Reset();

            return result;
        }
    }
}