using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DesingPatterns.FactoryMethode;
using System.Diagnostics;

public class ProductIncrementor : MonoBehaviour
{
    [SerializeField] private TMP_Text swords;
    [SerializeField] private TMP_Text armors;
    [SerializeField] private TMP_Text bows;

    public static int swordAmount = 0;
    public static int armorAmount = 0;
    public static int bowAmount = 0;

    public int SwordAmount { get { return swordAmount; } set 
        { 
            swordAmount = value;
            swords.text = swordAmount.ToString();
        } }
    public int ArmorAmount { get { return armorAmount; } set 
        {
            armorAmount = value;
            armors.text = armorAmount.ToString();
        } }
     public int BowAmount { get { return bowAmount; } set 
        {
            bowAmount = value;
            bows.text = bowAmount.ToString();
        } }

    private void OnEnable()
    {
        Creator.onProductCreate += SetExampleListeners;
    }

    private void OnDisable()
    {
        Creator.onProductCreate -= SetExampleListeners;
    }

    private void SetExampleListeners(string product)
    {
        switch (product)
        {
            case "{Sword}":
                SwordAmount++;
                break;
            case "{Armor}":
                ArmorAmount++;
                break;
            case "{Bow}":
                BowAmount++;
                break;
        }
    }
}
