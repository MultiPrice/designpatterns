using DesingPatterns.FactoryMethode;

namespace DesingPatterns.FactoryMethode.Examples
{
    public class Armor : IProduct
    {
        public string GetProduct()
        {
            return "{Armor}";
        }
    }
}
