using DesingPatterns.FactoryMethode;

namespace DesingPatterns.FactoryMethode.Examples
{
    public class Bow : IProduct
    {
        public string GetProduct()
        {
            return "{Bow}";
        }
    }
}
