using DesingPatterns.FactoryMethode;

namespace DesingPatterns.FactoryMethode.Examples
{
    public class Sword : IProduct
    {
        internal delegate void OnSwordCreate();
        internal static event OnSwordCreate onSwordCreate;

        public string GetProduct()
        {
            SwordCreated();
            return "{Sword}";
        }

        void SwordCreated()
        {
            if (onSwordCreate != null)
            {
                onSwordCreate();
            }
        }
    }
}


