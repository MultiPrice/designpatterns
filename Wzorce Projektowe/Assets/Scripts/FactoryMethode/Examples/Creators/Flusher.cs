namespace DesingPatterns.FactoryMethode.Examples
{
    public class Flusher : Creator
    {
        public override IProduct CreateProduct()
        {
            return new Bow();
        }
    }
}
