namespace DesingPatterns.FactoryMethode.Examples
{
    public class Armorer : Creator
    {
        public override IProduct CreateProduct()
        {
            return new Armor();
        }
    }
}
