
namespace DesingPatterns.FactoryMethode.Examples
{
    public class Blacksmith : Creator
    {
        public override IProduct CreateProduct()
        {
            return new Sword();
        }
    }
}

