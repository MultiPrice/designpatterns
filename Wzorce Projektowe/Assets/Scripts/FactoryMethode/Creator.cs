using UnityEngine;

namespace DesingPatterns.FactoryMethode
{
    public abstract class Creator: MonoBehaviour
    {
        // Only for example
        /*------->>> Events For Example <<<-------*/
        #region Events For Example
        internal delegate void OnProductCreate(string product);
        internal static event OnProductCreate onProductCreate;

        void ProductCreated(string product)
        {
            if (onProductCreate != null)
            {
                onProductCreate(product);
            }
        }
        #endregion

        public abstract IProduct CreateProduct();

        public void Demnstration()
        {
            IProduct product = CreateProduct();

            Debug.Log("Creator created new product... it was: " + product.GetProduct());

            // Only for example
            ProductCreated(product.GetProduct());
        }
    }
}

