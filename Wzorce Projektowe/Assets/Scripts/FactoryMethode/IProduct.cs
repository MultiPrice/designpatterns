namespace DesingPatterns.FactoryMethode
{
    public interface IProduct
    {
        string GetProduct();
    }
}