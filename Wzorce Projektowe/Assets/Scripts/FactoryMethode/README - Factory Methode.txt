Opis: Metoda wytw�rcza jest kreacyjnym wzorcem projektowym, 
	kt�ry udost�pnia interfejs do tworzenia obiekt�w w ramach klasy bazowej, 
	ale pozwala podklasom zmienia� typ tworzonych obiekt�w.

Zalety:
1. Unikasz �cis�ego sprz�gni�cia pomi�dzy tw�rc� a konkretnymi produktami.
2. Zasada pojedynczej odpowiedzialno�ci. Mo�esz przenie�� kod kreacyjny produkt�w w jedno miejsce programu, u�atwiaj�c tym samym utrzymanie kodu.
3. Zasada otwarte/zamkni�te. Mo�esz wprowadzi� do programu nowe typy produkt�w bez psucia istniej�cego kodu klienckiego.

Wady:
1. Kod mo�e si� skomplikowa�, poniewa� aby zaimplementowa� wzorzec, musisz utworzy� liczne podklasy. 
	W najlepszej sytuacji wprowadzisz �w wzorzec projektowy do ju� istniej�cej hierarchii klas kreacyjnych.

Najlepsze zastosowania: 
1. Stosuj Metod� Wytw�rcz� gdy nie wiesz z g�ry jakie typy obiekt�w pojawi� si� w twoim programie i jakie b�d� mi�dzy nimi zale�no�ci.
2. Korzystaj z Metody Wytw�rczej gdy zamierzasz pozwoli� u�ytkuj�cym tw� bibliotek� lub framework rozbudowywa� jej wewn�trzne komponenty.
3. Korzystaj z Metody wytw�rczej gdy chcesz oszcz�dniej wykorzysta� zasoby systemowe poprzez ponowne wykorzystanie ju� istniej�cych obiekt�w, 
	zamiast odbudowywa� je raz za razem.

Podsumowanie: Metoda wytw�rcza to bardzo u�yteczny wzorzec, kt�ry przydaje si� g��wnie gdy w projekcie mamy kilka klas zajmuj�cych si� wykonywaniem podobnych
	zada�, kt�re charakteryzuj� si� na sw�j spos�b. U�ywaj�c tego wzorca mo�na bez duplikowania kodu stworzy� jedn� klas� nadrz�dn� "Creator" i jeden interface
	po kt�rym bed� dziedziczy� efekty ko�cowe zada� wykonywanych przez klasy - "IProduct". Wtedy wewn�trz metody w klasie Creator zawieramy jedn� templatke 
	wykonania zadania, kt�rego efektem jest pochodna interfacu IProduct. 